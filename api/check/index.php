<?php
	
    $postData = json_decode( file_get_contents( 'php://input' ) );

    include '../fn.php';
    
    $uniqueMachineID = UniqueMachineID();

    $uniqueMachineID = $uniqueMachineID . $postData->guid;
    
	$string = file_get_contents("../data.json");
	$json = json_decode($string, true);
    $existed = "false";
    foreach ($json as $key => $value) {
        if($uniqueMachineID == $value) {
            $existed = "true";
        }        
    }
    
    echo(json_encode(array(
        "machineID" => $uniqueMachineID ,
        "quizzed" => $existed
    ))) ;

?>