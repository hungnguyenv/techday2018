<?php
	
    include '../fn.php';
    
    $postData = json_decode( file_get_contents( 'php://input' ) );
    $machineID = $postData->machineID ; 
    
	$string = file_get_contents("../redeem.json");
	$json = json_decode($string, true);
    $existed = "false";
    
    foreach ($json as $key => $value) {
        if($machineID == $value) {
            $existed = "true";
        }        
    }
    
    echo(json_encode(array(
        "machineID" =>$machineID,
        "redeemed" => $existed
    ))) ;

?>