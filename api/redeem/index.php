<?php
	header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    $postData = json_decode( file_get_contents( 'php://input' ) );

    $string = file_get_contents("../redeem.json");

    $machineID = $postData->machineID ; 

    $json = json_decode($string, true);
    
    $insertNew = "true";

    foreach ($json as $key => $value) {
        if($machineID == $value) {
            $insertNew = "false";
        }        
    }

    if ($insertNew == "true") {
        array_push($json, $machineID);
        file_put_contents("../redeem.json", json_encode($json));
    }

    echo(json_encode(array(
        "machineID" => $machineID ,
        "insertNew" => $insertNew
    ))) ;

?>