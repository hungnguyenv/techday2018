<?php
	
    $postData = json_decode( file_get_contents( 'php://input' ) );

    include '../fn.php';
    
    $uniqueMachineID = UniqueMachineID();

    $uniqueMachineID = $uniqueMachineID . $postData->guid;

    $string = file_get_contents("../data.json");

    $json = json_decode($string, true);
    
    array_push($json, $uniqueMachineID);

    file_put_contents("../data.json", json_encode($json));

    echo(json_encode(array(
        "machineID" => $uniqueMachineID ,
        "success" => "true"
    ))) ;

?>