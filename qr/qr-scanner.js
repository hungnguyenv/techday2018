'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var QrScanner = function () {
    function QrScanner(video, onDecode) {
        var _this = this;

        var canvasSize = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : QrScanner.DEFAULT_CANVAS_SIZE;

        _classCallCheck(this, QrScanner);

        this.$video = video;
        this.$canvas = document.createElement('canvas');
        this._onDecode = onDecode;
        this._active = false;

        this.$canvas.width = canvasSize;
        this.$canvas.height = canvasSize;
        this._sourceRect = {
            x: 0,
            y: 0,
            width: canvasSize,
            height: canvasSize
        };

        this.$video.addEventListener('canplay', function () {
            return _this._updateSourceRect();
        });
        this.$video.addEventListener('play', function () {
            _this._updateSourceRect();
            _this._scanFrame();
        }, false);
        this._qrWorker = new Worker(QrScanner.WORKER_PATH);
    }

    _createClass(QrScanner, [{
        key: '_updateSourceRect',
        value: function _updateSourceRect() {
            var smallestDimension = Math.min(this.$video.videoWidth, this.$video.videoHeight);
            var sourceRectSize = Math.round(2 / 3 * smallestDimension);
            this._sourceRect.width = this._sourceRect.height = sourceRectSize;
            this._sourceRect.x = (this.$video.videoWidth - sourceRectSize) / 2;
            this._sourceRect.y = (this.$video.videoHeight - sourceRectSize) / 2;
        }
    }, {
        key: '_scanFrame',
        value: function _scanFrame() {
            var _this2 = this;

            if (this.$video.paused || this.$video.ended) return false;
            requestAnimationFrame(function () {
                QrScanner.scanImage(_this2.$video, _this2._sourceRect, _this2._qrWorker, _this2.$canvas, true).then(_this2._onDecode, function (error) {
                    if (error !== 'QR code not found.') {
                        console.error(error);
                    }
                }).then(function () {
                    return _this2._scanFrame();
                });
            });
        }
    }, {
        key: '_getCameraStream',
        value: function _getCameraStream(facingMode) {
            var exact = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            var constraintsToTry = [{
                width: { min: 1024 }
            }, {
                width: { min: 768 }
            }, {}];

            if (facingMode) {
                if (exact) {
                    facingMode = { exact: facingMode };
                }
                constraintsToTry.forEach(function (constraint) {
                    return constraint.facingMode = facingMode;
                });
            }
            return this._getMatchingCameraStream(constraintsToTry);
        }
    }, {
        key: '_getMatchingCameraStream',
        value: function _getMatchingCameraStream(constraintsToTry) {
            var _this3 = this;

            if (constraintsToTry.length === 0) {
                return Promise.reject('Camera not found.');
            }
            return navigator.mediaDevices.getUserMedia({
                video: constraintsToTry.shift()
            }).catch(function () {
                return _this3._getMatchingCameraStream(constraintsToTry);
            });
        }
    }, {
        key: 'start',
        value: function start() {
            var _this4 = this;

            if (this._active) {
                return Promise.resolve();
            }
            this._active = true;
            clearTimeout(this._offTimeout);
            var facingMode = 'environment';
            return this._getCameraStream('environment', true).catch(function () {
                // we (probably) don't have an environment camera
                facingMode = 'user';
                return _this4._getCameraStream(); // throws if we can't access the camera
            }).then(function (stream) {
                _this4.$video.srcObject = stream;
                _this4._setVideoMirror(facingMode);
            }).catch(function (e) {
                _this4._active = false;
                throw e;
            });
        }
    }, {
        key: 'stop',
        value: function stop() {
            var _this5 = this;

            if (!this._active) {
                return;
            }
            this._active = false;
            this.$video.pause();
            this._offTimeout = setTimeout(function () {
                _this5.$video.srcObject.getTracks()[0].stop();
                _this5.$video.srcObject = null;
            }, 3000);
        }
    }, {
        key: '_setVideoMirror',
        value: function _setVideoMirror(facingMode) {
            // in user facing mode mirror the video to make it easier for the user to position the QR code
            var scaleFactor = facingMode === 'user' ? -1 : 1;
            this.$video.style.transform = 'scaleX(' + scaleFactor + ')';
        }
    }, {
        key: 'setGrayscaleWeights',
        value: function setGrayscaleWeights(red, green, blue) {
            this._qrWorker.postMessage({
                type: 'grayscaleWeights',
                data: { red: red, green: green, blue: blue }
            });
        }

        /* async */

    }], [{
        key: 'scanImage',
        value: function scanImage(imageOrFileOrUrl) {
            var sourceRect = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var worker = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var canvas = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
            var fixedCanvasSize = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
            var alsoTryWithoutSourceRect = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : false;

            var promise = new Promise(function (resolve, reject) {
                worker = worker || new Worker(QrScanner.WORKER_PATH);
                var timeout = void 0,
                    _onMessage = void 0,
                    _onError = void 0;
                _onMessage = function onMessage(event) {
                    if (event.data.type !== 'qrResult') {
                        return;
                    }
                    worker.removeEventListener('message', _onMessage);
                    worker.removeEventListener('error', _onError);
                    clearTimeout(timeout);
                    if (event.data.data !== null) {
                        resolve(event.data.data);
                    } else {
                        reject('QR code not found.');
                    }
                };
                _onError = function onError() {
                    worker.removeEventListener('message', _onMessage);
                    worker.removeEventListener('error', _onError);
                    clearTimeout(timeout);
                    reject('Worker error.');
                };
                worker.addEventListener('message', _onMessage);
                worker.addEventListener('error', _onError);
                timeout = setTimeout(_onError, 3000);
                QrScanner._loadImage(imageOrFileOrUrl).then(function (image) {
                    var imageData = QrScanner._getImageData(image, sourceRect, canvas, fixedCanvasSize);
                    worker.postMessage({
                        type: 'decode',
                        data: imageData
                    }, [imageData.data.buffer]);
                }).catch(reject);
            });

            if (sourceRect && alsoTryWithoutSourceRect) {
                return promise.catch(function () {
                    return QrScanner.scanImage(imageOrFileOrUrl, null, worker, canvas, fixedCanvasSize);
                });
            } else {
                return promise;
            }
        }

        /* async */

    }, {
        key: '_getImageData',
        value: function _getImageData(image) {
            var sourceRect = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var canvas = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            var fixedCanvasSize = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

            canvas = canvas || document.createElement('canvas');
            var sourceRectX = sourceRect && sourceRect.x ? sourceRect.x : 0;
            var sourceRectY = sourceRect && sourceRect.y ? sourceRect.y : 0;
            var sourceRectWidth = sourceRect && sourceRect.width ? sourceRect.width : image.width || image.videoWidth;
            var sourceRectHeight = sourceRect && sourceRect.height ? sourceRect.height : image.height || image.videoHeight;
            if (!fixedCanvasSize && (canvas.width !== sourceRectWidth || canvas.height !== sourceRectHeight)) {
                canvas.width = sourceRectWidth;
                canvas.height = sourceRectHeight;
            }
            var context = canvas.getContext('2d', { alpha: false });
            context.imageSmoothingEnabled = false; // gives less blurry images
            context.drawImage(image, sourceRectX, sourceRectY, sourceRectWidth, sourceRectHeight, 0, 0, canvas.width, canvas.height);
            return context.getImageData(0, 0, canvas.width, canvas.height);
        }

        /* async */

    }, {
        key: '_loadImage',
        value: function _loadImage(imageOrFileOrUrl) {
            if (imageOrFileOrUrl instanceof HTMLCanvasElement || imageOrFileOrUrl instanceof HTMLVideoElement || window.ImageBitmap && imageOrFileOrUrl instanceof window.ImageBitmap || window.OffscreenCanvas && imageOrFileOrUrl instanceof window.OffscreenCanvas) {
                return Promise.resolve(imageOrFileOrUrl);
            } else if (imageOrFileOrUrl instanceof Image) {
                return QrScanner._awaitImageLoad(imageOrFileOrUrl).then(function () {
                    return imageOrFileOrUrl;
                });
            } else if (imageOrFileOrUrl instanceof File || imageOrFileOrUrl instanceof URL || typeof imageOrFileOrUrl === 'string') {
                var image = new Image();
                if (imageOrFileOrUrl instanceof File) {
                    image.src = URL.createObjectURL(imageOrFileOrUrl);
                } else {
                    image.src = imageOrFileOrUrl;
                }
                return QrScanner._awaitImageLoad(image).then(function () {
                    if (imageOrFileOrUrl instanceof File) {
                        URL.revokeObjectURL(image.src);
                    }
                    return image;
                });
            } else {
                return Promise.reject('Unsupported image type.');
            }
        }

        /* async */

    }, {
        key: '_awaitImageLoad',
        value: function _awaitImageLoad(image) {
            return new Promise(function (resolve, reject) {
                if (image.complete && image.naturalWidth !== 0) {
                    // already loaded
                    resolve();
                } else {
                    var _onLoad = void 0,
                        _onError2 = void 0;
                    _onLoad = function onLoad() {
                        image.removeEventListener('load', _onLoad);
                        image.removeEventListener('error', _onError2);
                        resolve();
                    };
                    _onError2 = function onError() {
                        image.removeEventListener('load', _onLoad);
                        image.removeEventListener('error', _onError2);
                        reject('Image load error');
                    };
                    image.addEventListener('load', _onLoad);
                    image.addEventListener('error', _onError2);
                }
            });
        }
    }]);

    return QrScanner;
}();


QrScanner.DEFAULT_CANVAS_SIZE = 400;
QrScanner.WORKER_PATH = 'qr-scanner-worker.min.js';
