
var isProgress = false;

$('.qr-module').width(window.innerWidth / 1.1);
$('.qr-module').height(window.innerHeight / 2);

var videoElem = document.getElementById('qr-video');
var _qrScanner = new QrScanner(videoElem, setResult);

// ####### Web Cam Scanning #######
_qrScanner.start();

function setResult(result) {

	if(!isProgress) {
		
		$('.overlay').show();
		
		isProgress = true;
		
		$('.result').text(result);
		
		var dt = new Date().toISOString();
		$.ajax({
		  type: "POST",
		  url: 'https://ftel.akachains.io/createUserTransaction',
		  data: {
			"orgname": "ftel",
			"channelName": "ftelchannel",
			"chaincodeId": "ftel_cc",
			"args": [
				result.split('_')[0],
				"minus",
				"100",
				dt
			  ]
		  },
		  success: function(data) {
				var requestData = JSON.stringify({
					'machineID' : result.split('_')[0]					
				});
				
				$.ajax({
				  type: "POST",
				  url: '../api/redeem/',
				  data: requestData,
				  success: function(response) {
					  
					  if(response.insertNew == "false") {
						 isProgress = false;
						  alert('Redeem Failed. This token have been used');
					  } else if (response.insertNew == "true")  {
						  isProgress = false;
						  alert('Redeem Success!');
						  $('.result').text("");
					  }				  
					  
					$('.overlay').hide();
				  }
			  });
				
		  }

		});
		
	}
	
	
	
}
