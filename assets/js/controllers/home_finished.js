define(['app'], function (app) {
    app.controller('HomeFinishedCtrl', ['$scope', '$http', '$location', '$interval', '_appConfig', '_$utilities', '_$behaviors', '$sce', function($scope, $http, $location, $interval, _appConfig, _$utilities, _$behaviors, $sce) {
				
		_$behaviors.overlayTurnOff();
		
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
		   
		});
		
		$scope.$on('$locationChangeStart', function(event, next, current){
			
			if (next.indexOf('view_qr_code') !== -1) {		
				$location.path('/view_qr_code');
				return;
			} else if (next.indexOf('compatibility') !== -1) {		
				$location.path('/compatibility');
				return;
			} else {
				event.preventDefault();
				body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
		   
				});
			}
			
		});
		
		requirejs(['../js/phaser.min', '../js/animation'], function() {
			createAnimation();			
		});
		
		$scope.$on('$viewContentLoaded', function() {
			var record = JSON.parse(localStorage.getItem("record"));
			var qrContent = record.UserId + '_' + record.TxId;
			
			var qrcode = new QRCode(document.getElementById("qrcode"), {
				text: qrContent
			});
			
			_$behaviors.overlayTurnOn();
			
			$http({
			  	method: 'POST',
			  	url: 'api/checkredeem/',
				 data: JSON.stringify({
					"machineID" : record.UserId
				  }),
			}).then(function successCallback(response) {
				
				_$behaviors.overlayTurnOff();
				
			    if(response.data.redeemed == "false") {
					
					$scope.viewQRcode = function() {	
						_$behaviors.overlayTurnOn();
						$location.path('/view_qr_code');
					};		
		
				} else if (response.data.redeemed == "true") {
					$(".point span").text(0);
					$('.qr-code .btn').removeClass('btn-green');
					$('.qr-code .btn').text("Redeemed");					
				}
			  }, function errorCallback(response) {
					_$behaviors.overlayTurnOff();
			  });

		});		
		
		
  	}]);   	 
}); 