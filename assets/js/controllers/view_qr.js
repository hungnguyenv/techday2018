define(['app'], function (app) {
    app.controller('ViewQrCtrl', ['$scope', '$http', '$location', '$interval', '_appConfig', '_$utilities', '_$behaviors', '$sce', function($scope, $http, $location, $interval, _appConfig, _$utilities, _$behaviors, $sce) {
		
		_$behaviors.overlayTurnOff();
		
		var timer = null;
		
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
		   
		});
		
		$scope.$on('$locationChangeStart', function(event, next, current){
			if (next.indexOf('home_finished') !== -1) {		
				$location.path('/home_finished');
				return;
			} else if (next.indexOf('compatibility') !== -1) {		
				$location.path('/compatibility');
				return;
			} else {
				event.preventDefault();
				body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
		   
				});
			}
		});
		
		function checkRedeem(record) {
			_$behaviors.overlayTurnOff();
			$http({
			  	method: 'POST',
			  	url: 'api/checkredeem/',
				 data: JSON.stringify({
					"machineID" : record.UserId
				  })
			}).then(function successCallback(response) {			
				
			    if(response.data.redeemed == "false" || typeof(response) == 'undefined') {
					
				} else if (response.data.redeemed == "true") {
					clearInterval(timer);
					$location.path('/home_finished');
				}
			  }, function errorCallback(response) {
				
			  });
		}
		
		requirejs(['../js/phaser.min', '../js/animation'], function() {
			createAnimation();			
		});
		
		$scope.$on('$viewContentLoaded', function() {
			var record = JSON.parse(localStorage.getItem("record"));
			var qrContent = record.UserId + '_' + record.TxId;
			
			var qrcode = new QRCode(document.getElementById("qrcode"), {
				text: qrContent
			});		
			
			timer = setInterval(function(){ 
				checkRedeem(record);
			}, 5000);

		});
		
		
  	}]);   	 
}); 