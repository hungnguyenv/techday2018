define(['app'], function (app) {
    app.controller('SurveyCtrl', ['$scope', '$http', '$location', '$interval', '_appConfig', '_$utilities', '_$behaviors', '$sce', function($scope, $http, $location, $interval, _appConfig, _$utilities, _$behaviors, $sce) {
		
		_$behaviors.overlayTurnOff();
		
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
		   
		});
		
		function getGUID() {

			var nav = window.navigator;
			var screen = window.screen;
			var guid = nav.mimeTypes.length;			
			guid += nav.plugins.length;
			guid += screen.height || '';
			guid += screen.width || '';
			guid += screen.pixelDepth || '';

			return guid;
		};

		function doneSurvey(deviceId) {
			_$behaviors.overlayTurnOn();
			$http({
			  	method: 'POST',
			  	url: 'api/insert/',
			  	data: JSON.stringify({
					"guid" : deviceId
				  })
			}).then(function successCallback(response) {
				_$behaviors.overlayTurnOff();
			    $location.path('/success_survey');
			  }, function errorCallback(response) {
					
					_$behaviors.overlayTurnOff();
			  });
		}

		$scope.$on('$locationChangeStart', function(event, next, current){
			
			if (next.indexOf('success_survey') !== -1) {	
				_$behaviors.overlayTurnOn();
				$location.path('/success_survey');
				return;
			} else if (next.indexOf('compatibility') !== -1) {		
				$location.path('/compatibility');
				return;
			} else {
				event.preventDefault();
				body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
		   
				});
			}
		});
		
		requirejs(['../js/phaser.min', '../js/animation'], function() {
			createAnimation();			
		});
		
		var record = JSON.parse(localStorage.getItem("record"));
		var qrContent = record.UserId + '_' + record.TxId;
		
		var qrcode = new QRCode(document.getElementById("qrcode"), {
			text: qrContent
		});
		
		$('#link-qr').html("<span>" + qrContent + "</span>");

		$scope.validate = function() {
			var block = $(".block-inner") ;
			var valid = true;
			for (var i = 0; i < block.length; i++) {
				if($($(".block-inner")[i]).find(':checked').length == 0) {
					valid = false;
				}
			}
			if (valid) {
				new Fingerprint2().get(function(result, components) {			
					var deviceId = result + getGUID(); // a hash, representing your device fingerprint
					doneSurvey(deviceId);
				});
			} else {
				alert('Please complete all questions of the survey!');
			}
		};
		
  	}]);   	 
}); 