define(['app'], function (app) {
    app.controller('SuccessSurveyCtrl', ['$scope', '$http', '$location', '$interval', '_appConfig', '_$utilities', '_$behaviors', '$sce', function($scope, $http, $location, $interval, _appConfig, _$utilities, _$behaviors, $sce) {
		_$behaviors.overlayTurnOff();
		
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
		   
		});
		
		$scope.$on('$locationChangeStart', function(event, next, current){
			
			if (next.indexOf('home_finished') !== -1) {		
				$location.path('/home_finished');
				return;
			} else if (next.indexOf('compatibility') !== -1) {		
				$location.path('/compatibility');
				return;
			} else {
				event.preventDefault();
				body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
		   
				});
			}
		});
		
		requirejs(['../js/phaser.min', '../js/animation'], function() {
			createAnimation();			
		});
		
		$scope.redirecTo = function() {
			_$behaviors.overlayTurnOn();
			$location.path('/home_finished');
			
		};
		
  	}]);   	 
}); 