define(['app'], function (app) {
    app.controller('HomeCtrl', ['$scope', '$http', '$location', '$interval', '_appConfig', '_$utilities', '_$behaviors', '$sce', function($scope, $http, $location, $interval, _appConfig, _$utilities, _$behaviors, $sce) {			
			
		_$behaviors.overlayTurnOn();
		
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
		   
		});		
		
		function getGUID() {

			var nav = window.navigator;
			var screen = window.screen;
			var guid = nav.mimeTypes.length;			
			guid += nav.plugins.length;
			guid += screen.height || '';
			guid += screen.width || '';
			guid += screen.pixelDepth || '';

			return guid;
		};

		function getUserTransactionByTxId(record) {
			
			_$behaviors.overlayTurnOff();
			
			localStorage.setItem("record", JSON.stringify(record));			
			$scope.token = record.UserId + '_' + record.TxId;
			new QRCode(document.getElementById("qrcode"), {
				text: $scope.token
			});
			$("#link-qr").text($scope.token);
			$(".point span").text(record.Amount);
		}
		
        function getAllUserTransaction(machineID, dt) {
            
            $.ajax({
			  type: "POST",
			  url: 'https://ftel.akachains.io/getAllUserTransaction',
			  data: {
                  "orgname": "ftel",
                  "channelName": "ftelchannel",
                  "chaincodeId": "ftel_cc"
                },
                success: function(result) {
                   
					var record = result.data[result.data.length-1];
					var find = null;
					for (var i in result.data){
						if(typeof(result.data[i].UserId) !== 'undefined'
						&& typeof(result.data[i].TransactionDatetime) !== 'undefined') {
							if(result.data[i].UserId == machineID && result.data[i].TransactionDatetime == dt) {
								find = result.data[i] ;							
							}
						}
						
					}
					if(find) {
						console.log(find);
						getUserTransactionByTxId(find);	
					} else {
						getAllUserTransaction(machineID, dt)
					}
					
			  }

			});
        }
        
		function createUserTransaction(machineID) {			
			if (localStorage.getItem("record")) {
				getUserTransactionByTxId(JSON.parse(localStorage.getItem("record")));				
			}
			else {
				var dt = new Date().toISOString();
				$.ajax({
				  type: "POST",
				  url: 'https://ftel.akachains.io/createUserTransaction',
				  data: {
					"orgname": "ftel",
					"channelName": "ftelchannel",
					"chaincodeId": "ftel_cc",
					"args": [
						machineID,
						"plus",
						"100",
						dt
					  ]
				  },
				  success: function(result) {
					  getAllUserTransaction(machineID, dt);
				  }

				});

			}
		}

		new Fingerprint2().get(function(result, components) {
			
			var deviceId = result + getGUID(); // a hash, representing your device fingerprint
				
			$http({
			  	method: 'POST',
			  	url: 'api/check/',
			  	 data: JSON.stringify({
						"guid" : deviceId
					  }),
			}).then(function successCallback(response) {
			    if(response.data.quizzed == "true") {
					$location.path('/home_finished');
					return;
			    } else {
			    	createUserTransaction(response.data.machineID);
			    }
			  }, function errorCallback(response) {
					_$behaviors.overlayTurnOff();
			  });

		});


		$scope.$on('$locationChangeStart', function(event, next, current){
			
			if (next.indexOf('survey') !== -1) {		
				$location.path('/survey');
				return;
			} else if (next.indexOf('home_finished') !== -1) {		
				$location.path('/home_finished');
				return;
			} else if (next.indexOf('compatibility') !== -1) {		
				$location.path('/compatibility');
				return;
			} else {
				event.preventDefault();
				body.stop().animate({scrollTop:0}, 500, 'swing', function() { 
		   
				});
			}
		});

		requirejs(['../js/phaser.min', '../js/animation'], function() {
			createAnimation();			
		});
 
		$scope.startSurvey = function() {
			_$behaviors.overlayTurnOn();
			$location.path('/survey');
		};
		
		
  	}]);   	 
}); 