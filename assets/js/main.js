require.config({
    baseUrl: "assets/js", 
	waitSeconds : 50,	
    paths: {
        'angular': '../components/angular.min',
        'angular-route': '../components/angular-route.min',
		'angularAMD': '../components/angularAMD',
		'jquery': 'jquery-3.2.1.min',
		'qrcode': 'qrcode.min'
    },
    shim: {
		'qrcode': ['jquery'],			
		'angularAMD': ['angular'], 
		'angular-route': ['angular'] 
	},
	deps: ['app']
});
