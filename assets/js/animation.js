var game;
var platform;

// once the window loads...
function createAnimation() {
	var config = {
		// render type
		type: Phaser.AUTO,

		// Parent
		parent: 'top-chain',

		// game width, in pixels
		width: window.innerWidth,

		// game height, in pixels
		height: $('#top-chain').height() * 1.1,

		// transparent background color
		transparent: true,

		physics: {
			default: 'arcade'
		},
		scene: {
			preload: preload,
			create: create
		}
	};

	game = new Phaser.Game(config);


	function preload ()
	{
		platform = this;

		platform.load.atlas('flares', 'assets/animations/flares.png', 'assets/animations/flares.json');
		platform.load.image('logo', 'assets/img/logo.png');
	}

	function create ()
	{
		var textures = platform.textures;

		var container = {
			x: Math.floor($('#top-chain').position().left),
			y: Math.floor($('#top-chain').position().top),
			width: game.config.width,
			height: $('#top-chain').height()
		};

		var logoSource = {
			getRandomPoint: function (vec)
			{
				try {
					do
					{
						var x = Phaser.Math.Between(0, container.width);
						var y = Phaser.Math.Between(0, container.height);
						var pixel = textures.getPixel(x, y, 'logo');
					} while (pixel.alpha < 255);

					return vec.setTo(x + container.x, y + container.y);
				} catch(ex) {

				}
			}
		};

		var particles = this.add.particles('flares');

		particles.createEmitter({
			frame: 'white',       
			lifespan: 5000,
			gravityY: 10,
			speedX: { min: -game.config.width / 4, max: game.config.width / 2},
			speedY: { min: -game.config.width / 4, max: game.config.width / 2},
			scale: { start: 0, end: 0.25, ease: 'Quad.easeOut' },
			alpha: { start: 1, end: 0, ease: 'Quad.easeIn' },
			blendMode: 'ADD',
			emitZone: { type: 'random', source: logoSource }
		});
	}
}