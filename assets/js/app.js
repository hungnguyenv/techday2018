
define(['angularAMD', 'angular-route', 'jquery', 'qrcode'], function (angularAMD) {
	
	var app = angular.module("techApp", ['ngRoute']);
	
	/*app.run(['$templateCache', function ($templateCache) {
		$templateCache.removeAll();
	}]);*/
	
	app.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
		
		$httpProvider.defaults.withCredentials = true;
		
		$routeProvider
		.when('/', angularAMD.route({
			templateUrl: 'templates/home.html',
			controller: 'HomeCtrl',
			controllerUrl: 'controllers/home',
			resolve: {
				bodyClass: function(){
					angular.element(document.querySelector('body')).removeAttr('class');
					angular.element(document.querySelector('body')).addClass('page-home');					
				}
			}
		}))
		.when('/survey', angularAMD.route({
			templateUrl: 'templates/survey.html',
			controller: 'SurveyCtrl',
			controllerUrl: 'controllers/survey',
			resolve: {
				bodyClass: function(){
					angular.element(document.querySelector('body')).removeAttr('class');
					angular.element(document.querySelector('body')).addClass('page-survey');					
				}
			}
		}))		
		.when('/success_survey', angularAMD.route({
			templateUrl: 'templates/success_survey.html',
			controller: 'SuccessSurveyCtrl',
			controllerUrl: 'controllers/success_survey',
			resolve: {
				bodyClass: function(){
					angular.element(document.querySelector('body')).removeAttr('class');
					angular.element(document.querySelector('body')).addClass('page-survey success');					
				}
			}
		}))	
		.when('/home_finished', angularAMD.route({
			templateUrl: 'templates/home_finished.html',
			controller: 'HomeFinishedCtrl',
			controllerUrl: 'controllers/home_finished',
			resolve: {
				bodyClass: function(){
					angular.element(document.querySelector('body')).removeAttr('class');
					angular.element(document.querySelector('body')).addClass('page-home');					
				}
			}
		}))
		.when('/view_qr_code', angularAMD.route({
			templateUrl: 'templates/view_qr_code.html',
			controller: 'ViewQrCtrl',
			controllerUrl: 'controllers/view_qr',
			resolve: {
				bodyClass: function(){
					angular.element(document.querySelector('body')).removeAttr('class');
					angular.element(document.querySelector('body')).addClass('page-home');					
				}
			}
		}))
		.when("/compatibility", angularAMD.route({
			templateUrl: 'templates/compatibility.html',
			controller: 'CompatibilityCtrl',
			controllerUrl: 'controllers/compatibility'	,
			resolve: {
				bodyClass: function(){
					angular.element(document.querySelector('body')).removeAttr('class');
					angular.element(document.querySelector('body')).addClass('page-compatibility');		
				}
			}
		}))
		
	}]);



	/***************** Global Vars *****************/

	app.factory('_appConfig', function() {
		return {
			domain : 'http://m-optimize-public.apps.cloudhub.com.vn/onef',
			quizAPIPath : '/Quiz/api/Public/',
			xFilesAPIPath : '/XFiles/public-api/Public/',
			accountAPIPath : '/public-api/account/',
			rankingAPIPath : '/Ranking/public-api/RankingResult/',
			gameAPIPath : '/Gamification/api/Public/',
			accessKey : '5fba3891dc5d46959437e71c346d02e3',
			secretKey : '0yURl//H1OGuM/12pO8x9dHIWvkF/84kFf4tk8hxuKU=',			
			companyCode: 'MSC',
			applicationCode: 'MYE',			
			isProcessing: false
		};
	});

	/***************** //Global Vars *****************/

	/***************** Data Shared Service *****************/	

	app.service('_$serviceSharedData', function() {
		var sharedData = {
			globalData : {
				user: null,				
				sessionCode : null				
			},
			dynamicData : []
		};		

		var addData = function(globalData, localData) {		
				
				sharedData.dynamicData = [];
				sharedData.dynamicData.push(localData);
			};
			
			var getData = function(){
				return sharedData;
			}
			
			return {
				addData: addData,
				getData: getData
			};
		});	

	/***************** //Data Shared Service *****************/

	/***************** Global Behaviors *****************/
	
	// Enter Key Press
	app.directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.ngEnter, {'event': event});
                    });

                    event.preventDefault();
                }
            });
        };
    });
	
	// Show. Hide Overlay	
	app.service('_$behaviors', function() {
		// Show, hide overlay loading
		this.overlayTurnOn = function() {			
			var overlayDOMObj = angular.element(document.querySelector('.overlay'));
			overlayDOMObj.addClass('show');
		};
		this.overlayTurnOff = function() {
			var overlayDOMObj = angular.element(document.querySelector('.overlay'));
			overlayDOMObj.removeClass('show');
		}
	});

	/***************** //Global Behaviors *****************/

	/***************** Utilities *****************/

	app.service('_$utilities', function() {

		// Function Convert string data to datetime format
		this.convertDateTime = function(strObj) {
			
			var dtObj = new Date(strObj);
			
			return date;
		};
		
		// Function to check the current browser support local storage or not
		this.isSupportHtml5Storage = function() {
			try {
				return 'localStorage' in window && window['localStorage'] !== null;
			} catch(e) {
				return false;
			}
		};
		
		// Function to check the current browser type and version
		this.getSystemInfo = function() {
			
			var module = {
				options: [],
				header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor, window.opera],
				dataos: [
				{ name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
				{ name: 'Windows', value: 'Win', version: 'NT' },
				{ name: 'iPhone', value: 'iPhone', version: 'OS' },
				{ name: 'iPad', value: 'iPad', version: 'OS' },
				{ name: 'Kindle', value: 'Silk', version: 'Silk' },
				{ name: 'Android', value: 'Android', version: 'Android' },
				{ name: 'PlayBook', value: 'PlayBook', version: 'OS' },
				{ name: 'BlackBerry', value: 'BlackBerry', version: '/' },
				{ name: 'Macintosh', value: 'Mac', version: 'OS X' },
				{ name: 'Linux', value: 'Linux', version: 'rv' },
				{ name: 'Palm', value: 'Palm', version: 'PalmOS' }
				],
				databrowser: [
				{ name: 'Chrome', value: 'Chrome', version: 'Chrome' },
				{ name: 'Firefox', value: 'Firefox', version: 'Firefox' },
				{ name: 'Safari', value: 'Safari', version: 'Version' },
				{ name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
				{ name: 'Opera', value: 'Opera', version: 'Opera' },
				{ name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
				{ name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
				],
				init: function () {
					var agent = this.header.join(' '),
					os = this.matchItem(agent, this.dataos),
					browser = this.matchItem(agent, this.databrowser);
					
					return { os: os, browser: browser };
				},
				matchItem: function (string, data) {
					var i = 0,
					j = 0,
					html = '',
					regex,
					regexv,
					match,
					matches,
					version;
					
					for (i = 0; i < data.length; i += 1) {
						regex = new RegExp(data[i].value, 'i');
						match = regex.test(string);
						if (match) {
							regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
							matches = string.match(regexv);
							version = '';
							if (matches) { if (matches[1]) { matches = matches[1]; } }
							if (matches) {
								matches = matches.split(/[._]+/);
								for (j = 0; j < matches.length; j += 1) {
									if (j === 0) {
										version += matches[j] + '.';
									} else {
										version += matches[j];
									}
								}
							} else {
								version = '0';
							}
							return {
								name: data[i].name,
								version: parseFloat(version)
							};
						}
					}
					return { name: 'unknown', version: 0 };
				}
			};
			
			return module.init();
		};
		
		// Function convert seconds to inutes and seconds
		this.secondsToMinutes = function(miliSeconds) {
			
			var seconds = Math.round(miliSeconds / 1000);
			
			var minutes = Math.floor(seconds / 60);
			var remainSeconds = seconds - (minutes * 60);
			var time = {
				minutes : minutes,
				seconds : remainSeconds				
			};
			
			return time;
		};
		
		// Function convert minutes to seconds
		this.minutesToSeconds = function(minutes) {
			var seconds = Math.floor(minutes * 60);		
			return seconds;
		};

		// Function set mask for number
		this.setMaskForTime = function(number) {
			if (number<10) {
				number = "0" + number;
			}
			return number ;
		}		
		
		// Function filter phone number
		this.filterPhone = function(num) {
			
			if (typeof(num)=='undefined') {
				return "";
			}
			var pattern = ['(84)','(+84)','+84','0084'];
			for (var i = 0; i < pattern.length ; i++) {
				if (num.indexOf(pattern[i]) == 0) {
					return num.replace(pattern[i],"0");
				}
			}
			return num;
		} 
		
		// Disable Console Log
		this.disableConsoleLog = function(number) {
			console.log = function () {};	
		};
		
	});	

	/***************** //Utilities *****************/

	/***************** Master rules validate needed before we're going to load the pageLoading  *****************/

	angular.element(document).ready(function () {

		var injector = angular.injector(['ng', 'techApp']);	
		var utilities = injector.get('_$utilities');		
		var system = utilities.getSystemInfo();

		console.log(system);
		
		//utilities.disableConsoleLog();
		
		// Policies check before give an access to the page
		if(!utilities.isSupportHtml5Storage() 
			|| (system.browser.name === 'Internet Explorer')
			|| (system.os.name === "Macintosh" && system.browser.name !== "Safari")
			|| (system.os.name === "Windows" && system.browser.name !== "Chrome")
			|| (system.os.name === "Android" && system.browser.name !== "Chrome")
			|| (system.os.name === "iPhone" && !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/) === false)
			|| (system.os.name === "iPad" && !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/) === false)) {

			app.run(['$rootScope', '$location', function($rootScope, $location) {
				
				$rootScope.$on('$routeChangeSuccess', function () {			    
					$location.path("/compatibility");
				});
			}]);			
		} else if (!(window.ActiveXObject) && "ActiveXObject" in window === true || system.browser.name === 'Internet Explorer') {
			
			// IE Edge
			angular.element(document.querySelector('html')).addClass('ie');
			
		}
		
			
	});

	/***************** //Master rules validate needed before we're going to load the pageLoading  *****************/

	return angularAMD.bootstrap(app);

});

